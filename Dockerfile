FROM python:3.9

RUN pip install \
    streamlit==1.2.0 \
    SQLAlchemy==1.3.13 \
    pandas==1.2.3 \
    pytz==2019.1 \
    dataclasses \
    alembic==1.5.4 \
    pyyaml==5.4.1 \
    kafka-python==2.0.2 \
    pydantic \
    uszipcode

RUN mkdir /app
COPY . /app

WORKDIR /app

CMD ["python", "-m", "src.consumer"]








