from pydantic import BaseModel


class UserSerializer(BaseModel):
    user_id: str
    timestamp: str
    country: str
    post_code: str
    city: str
    state: str

# TODO: Add validators
