from sqlalchemy.orm import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    Sequence)

Base = declarative_base()


class UserModel(Base):
    __tablename__ = 'users'

    id = Column(Integer, Sequence('user_id_seq'), primary_key=True)
    user_id = Column(String)
    timestamp = Column(String)
    country = Column(String)
    post_code = Column(String)
    city = Column(String)
    state = Column(String)
