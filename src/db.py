import sqlite3

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def create_connection(db_file="/tmp/blockchain.db"):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Exception as e:
        print(e)

    return conn


engine = create_engine("sqlite:////tmp/blockchain.db")


Session = sessionmaker(bind=engine)
session = Session()
