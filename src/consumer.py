import re
import json

from typing import Tuple
from kafka import KafkaConsumer
from uszipcode import SearchEngine

from src.db import session
from src.models import UserModel
from src.serializers import UserSerializer

from src.static.state_abbrev import us_state_to_abbrev


sr = SearchEngine()


PATH_TO_CSV_FILE = "/tmp/sample_us_users.csv"

kc = KafkaConsumer('test',
                   bootstrap_servers=['rc1a-428a40lift0qiaba.mdb.yandexcloud.net:9091'],
                   sasl_plain_username='test-consumer',
                   sasl_plain_password='lolkek123',
                   security_protocol='SASL_SSL',
                   sasl_mechanism='SCRAM-SHA-512',
                   ssl_certfile='/tmp/YandexCA.crt',
                   ssl_cafile='/tmp/YandexCA.crt',
                   )


def parse_message(msg: bytes) -> Tuple[str, str, dict]:
    """
    Parses initial message in bytes and returns

    """
    msg = msg.value.decode()
    PATTERN = '(.*),"({.*})",(.*)'
    result = re.search(pattern=PATTERN, string=msg)
    user_id = result.group(1)
    address = result.group(2)
    ts = result.group(3)
    try:
        address = json.loads(address.replace("'", '"').replace('None', 'null').replace('"s ', "'s "))
    except json.decoder.JSONDecodeError:
        try:
            # if smth extraordinary happens, we add one more replace action
            address = json.loads(address.replace("'", '"').
                                 replace('None', 'null').
                                 replace('"s ', "'s ").
                                 replace('""', '"'))
        except json.decoder.JSONDecodeError:
            print(address)
            raise json.decoder.JSONDecodeError
    return user_id, ts, address


def process_state(address, search_result):
    """
    Takes address and process state field inplace

    # Sometimes state an abbrev, sometimes it's a full name. For the sake of uniformity
    # it's better to use search by post and use abbrev from their.
    # Of course, we can apply another level of validation and compare inferred state with given state,
    # But you told, that the solution shouldn't be very polished :)
    """

    if (search_result is not None) and (search_result.state is not None):
        address["state"] = search_result.state
    elif address["state"] in us_state_to_abbrev:
        address["state"] = us_state_to_abbrev[address["state"]]
    elif address["state"] is None:
        address["state"] = "MISSING"
    else:
        address["state"] = address["state"].upper().replace("US-", "").strip()


def process_city(address, search_result):
    """
    City name
    We can validate searched cities and cities from csv, but that would be too extensive for test case
    Instead of this, we would only replace csv city by searched city if csv city is corrupted.
    Sometimes cities for search are not the right choice (Oh vice versa - maybe they are more objective,
    than user's choice),
    So we are not replcaing user's cities blindly - we do it only when necessary.
    """
    if search_result is not None:
        if len(address["city"]) < 3:  # There are no cities shorter than 3 symbols
            address["city"] = search_result.city
        if (address["city"].upper() == "NONE") | (address["city"].upper() == "NULL"):
            address["city"] = search_result.city
    else:
        address["city"] = address["city"].strip()


def process_message(msg):
    """
    Processing message from Kafka.
    Main goal here is to parse address json and make necessary imputations

    :return: parsed dict with values equivalent to fields for relational database
    """
    # None -> null, so json.loads can parse it correctly "s -> 's because apostrophe is being substituted and we need to return it
    # Id and timestamp string sizes are fixed, so we don't need clever parsing here.
    user_id, ts, address = parse_message(msg)

    # Enrich / Validate Data
    search_result = sr.by_zipcode(address["postCode"])

    process_state(address, search_result)
    process_city(address, search_result)

    user = UserSerializer(user_id=user_id,
                          timestamp=ts,
                          country=address["country"],
                          post_code=address["postCode"],
                          city=address["city"],
                          state=address["state"])

    user = UserModel(**user.dict())
    session.add(user)
    # If we want to optimize network performance, then we would use buffers,
    # but as we are working with file DB - there's no need in optimiation
    session.commit()


# Consumers Iterator
for msg in kc:
    process_message(msg)
