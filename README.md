```
dagster

│ shell    
│ src    
    └─── static
        │ state_abbrev.py  # File with static naming info
    │ db.py                # Database conection   
    │ models.py            # DataBase user model
    │ serializers.py       # Data Validators
    │ consumer             # Entrypoint 
```